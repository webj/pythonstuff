# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 12:03:13 2020

@author: Bedag
"""
import unittest
from euler_problems import Prb003PrimeFactor

class UnitEulerTest(unittest.TestCase):
    
    def test_prb003(self):        
        prob003 = Prb003PrimeFactor()
        self.assertEqual(max(prob003.largestprimefactor(600851475143 , [])), 6857)

if __name__ == '__main__':
    unittest.main()