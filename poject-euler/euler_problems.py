# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 12:26:38 2020
Collections of Euler Problems
@author: Bedag
"""

from euler_helper import EulerHelper


class Prb003PrimeFactor:   
        
    def __intit__(self):
         self.params = []
        
    def largestprimefactor(self, test, result):      
        helper = EulerHelper()
        if helper.isprime(test):
            result += [test]
        else:
            i = 2 
            while test % i > 0 and int(test/2)+1>=i: 
                i += 1
            result += [i]
            self.largestprimefactor(int(test/i), result)                
        return result