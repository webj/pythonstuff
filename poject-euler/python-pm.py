# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 22:07:52 2020

@author: Bedag
"""

num = 600851475143
i = 1
numVec = []
while i < num:
    for j in range(2,num):
        if i % j != 0:
            pass
        else:
            numVec.append(i)
    i = i+1
    
primeNum = []
for i in range(1, len(numVec)-1):
    if (numVec[i] != numVec[i-1]) &(numVec[i] != numVec[i+1]):
        primeNum.append(numVec[i])
        
divisors = []
for num in primeNum:
    if 600851475143 % num == 0:
        divisors.append(num)
print(max(divisors))